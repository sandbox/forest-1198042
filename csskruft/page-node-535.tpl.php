<?php require 'header.tpl.php'; ?>
<?php // if (!isset($is_front_page)){ };?>
<!--Begin Hero Section-->

		<section id="hero">

			<div class="texture">

				<div id="hero_background">	
					
					<div class="main">
                    
                    <?php print $hero;?>

						<div id="bubble">
							<img src="<?php print base_path().path_to_theme() . '/images/bubble.png'?>" alt="">
						</div> <!-- end #bubble -->
					</div> <!-- end .main -->

					<div class="clearfix"></div>

				</div>

			</div>	
		</section>
	
<!-- Print main Drupal content -->

<!-- main content removed as per Development Change request > Needed for Alpha > 13 -->
    
<!--Begin Footer-->
		<footer>
			<div id="footer">
				<div class="texture">

				<section id="sub_footer">
					<div class="texture3">
						<div class="main">
							<div id="about">
								<?php print $triptych1;?>
							</div>
							<div id="contactus">
								<?php print $triptych2;?>
							</div>
							<div id="signup">
								<?php print $triptych3;?>
							</div>

							<!--End Sign Up for Newsletter Form-->
		    			</div>

						<div class="clearfix"></div>

					</div>	

				</section>

					<div class="main">
						<?php print $footer_message . $footer ?>	

			<!-- temporarily including sponsor logos in template. this should be moved to drupal footer. -->

							<h6>Partners:</h6>

						<ul>
								<li><id="mbp"><a href="http://www.mbpo.org/"><img src="<?php print base_path().path_to_theme() . '/images/logo_bp.png'?>" alt="Manhattan Borough President"/></a></li>
								<li id="openplans"><a href="http://www.openplans.org"><img src="<?php print base_path().path_to_theme() . '/images/logo_openplans.png'?>" alt="Open Plans"/></a></li>
								<li id="mnn"><a href="http://www.mnn.org"><img src="<?php print base_path().path_to_theme() . '/images/logo_mnn.png'?>" alt="MNN"/></a></li>
<li id="infinopolis"><a href="http://infinopolis.com/"><img src="<?php print base_path().path_to_theme() . '/images/logo_infinopolis.png'?>" alt="Infinopolis"/></a></li>								
<li id="nycwiki"><a href="http://nycwiki.org/wiki/Main_Page"><img src="<?php print base_path().path_to_theme() . '/images/logo_nycwiki.png'?>" alt="NYC Wiki"/></a></li>
								<li id="clickfix"><a href="http://www.seeclickfix.com/"><img src="<?php print base_path().path_to_theme() . '/images/logo_seeclickfix.png'?>" alt="SeeClickFix"/></a></li>
						</ul>

						<p class="copy">&copy; Copyright 2006 The Manhattan Borough President’s Office</p>

						<div id="balance"><p>Design &amp; Front-End Development by</p> <a href="http://www.balance.coop"><img src="<?php print base_path().path_to_theme() . '/images/logo_balance.png'?>" alt="Balance"/></a></div>

						<div class="clearfix"></div>	
					</div>
			</div>
			</div>
		</footer>
<!--End Footer-->


	</div>
	</div>
<?php
print $closure
?>

<!-- Javascript at the bottom for fast page loading -->

	  <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
	  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
	  <script>!window.jQuery && document.write(unescape('%3Cscript src="js/libs/jquery-1.4.2.js"%3E%3C/script%3E'))</script>
	
	<!--Geolocation/Map JS (This only needs to be included on the homepage)-->
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="<?php print base_path().path_to_theme() . '/js/google.maps.in_poly.js'?>"></script>
	<script type="text/javascript" src="<?php print base_path().path_to_theme() . '/js/community_boards.js'?>"></script>
	<script>
	$(document).ready( function() { initializeMap(); } )
	</script>

	<!--Slideshow, Bubble Animation & Main Dropdown (This is global)-->
	<script src="<?php print base_path().path_to_theme() . '/js/plugins.js'?>"></script>
	<script src="<?php print base_path().path_to_theme() . '/js/script.js'?>"></script>

	  <!--[if lt IE 7 ]>
	    <script src="js/libs/dd_belatedpng.js"></script>
	    <script> DD_belatedPNG.fix('img, .png_bg'); //fix any <img> or .png_bg background-img </script>
	  <![endif]-->

	</body>
	</html>
