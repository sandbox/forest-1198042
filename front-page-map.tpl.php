<!--Begin Map Section--> 
	
	<h3><img src="<?php print base_path().path_to_theme() . '/images/icon_apartment.png'?>"/> Get Involved in Your Neighborhood</h3> 
		
		<div id="communitylist" class="cb_group_list"> 
						
			<div id="geolocation"> 
				<img src="<?php print base_path().path_to_theme() . '/images/marker.png'?>" /> 
				<p id="statement">We have you located in <strong>Hamilton Heights</strong> with is located in <strong><a href="#">Community Board 9</a></strong>.</p> 
				
				<div class="viewcommunity"><a href="#">View Community Page</a></div> 
				</div> 
				
				<div id="communityboards"> 
					<p>Not your Neighborhood? Pick from the list below.</p> 
					<h4><a href="#">Community Boards</a></h4> 
				<ul id=""> 
					<li id="one"><div class="numbers">1</div> <p><a href="/communityboard1">Lower Manhattan</a></p> <div class="forum_link"><a href="/communityboard1#forums">Neighborhood Forum</a></div></li> 
					<li id="two"><div class="numbers">2</div> <p><a href="/communityboard2">Greenwich Village &amp; SoHo</a></p> <div class="forum_link"><a href="/communityboard2#forums">Neighborhood Forum</a></div></li> 
					<li id="three"><div class="numbers">3</div> <p><a href="/communityboard3">East Village, Lower East Side &amp; Chinatown</a></p> <div class="forum_link"><a href="/communityboard3#forums">Neighborhood Forum</a></div></li> 
					<li id="four"><div class="numbers">4</div> <p><a href="/communityboard4">Chelsea &amp; Hell's Kitchen</a></p> <div class="forum_link"><a href="/communityboard4#forums">Neighborhood Forum</a></div></li> 
					<li id="five"><div class="numbers">5</div> <p><a href="/communityboard5">Midtown</a></p> <div class="forum_link"><a href="/communityboard5#forums">Neighborhood Forum</a></div></li> 
					<li id="six"><div class="numbers">6</div> <p><a href="/communityboard6">Murray Hill, Gramercy Park &amp; Turtle Bay</a></p> <div class="forum_link"><a href="/communityboard6#forums">Neighborhood Forum</a></div></li> 
					<li id="seven"><div class="numbers">7</div> <p><a href="/communityboard7">Upper West Side</a></p> <div class="forum_link"><a href="/communityboard7#forums">Neighborhood Forum</a></div></li> 
					<li  id="eight"><div class="numbers">8</div> <p><a href="/communityboard8">Upper East Side</a></p> <div class="forum_link"><a href="/communityboard8#forums">Neighborhood Forum</a></div></li> 
					<li id="nine"><div class="numbers">9</div> <p><a href="/communityboard9">Hamilton Heights &amp; West Harlem</a></p> <div class="forum_link"><a href="/communityboard9#forums">Neighborhood Forum</a></div></li> 
					<li id="ten"><div class="numbers">10</div> <p><a href="/communityboard10">Harlem</a></p> <div class="forum_link"><a href="/communityboard10#forums">Neighborhood Forum</a></div></li> 
					<li  id="eleven"><div class="numbers">11</div> <p><a href="/communityboard11">East Harlem</a></p> <div class="forum_link"><a href="/communityboard11#forums">Neighborhood Forum</a></div></li> 
					<li id="twelve"><div class="numbers">12</div> <p><a href="/communityboard12">Washington Heights &amp; Inwood</a></p> <div class="forum_link"><a href="/communityboard12#forums">Neighborhood Forum</a></div></li> 
					</ul> 
				</div> 
						
			</div> 
			
			<div id="map_canvas"> 
				</div> 
 
<!--End Map Section--> 