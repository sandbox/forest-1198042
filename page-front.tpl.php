<?php require 'header.tpl.php'; ?>
<?php // if (!isset($is_front_page)){ };?>
<!--Begin Hero Section-->

		<section id="hero">

			<div class="texture">

				<div id="hero_background">	
					
					<div class="main">
                    
                    <?php print $hero;?>

						<div id="bubble">
							<img src="<?php print base_path().path_to_theme() . '/images/bubble.png'?>" alt="">
						</div> <!-- end #bubble -->
					</div> <!-- end .main -->

					<div class="clearfix"></div>

				</div>

			</div>	
		</section>
	
<!-- Print main Drupal content -->

<!-- main content removed as per Development Change request > Needed for Alpha > 13 -->
    
<!--Begin Footer-->
		<?php require('footer.tpl.php'); ?> 
<!--End Footer-->


	</div>
	</div>
<?php
print $closure
?>

<!-- Javascript at the bottom for fast page loading -->

	  <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
	  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
	  <script>!window.jQuery && document.write(unescape('%3Cscript src="js/libs/jquery-1.4.2.js"%3E%3C/script%3E'))</script>
	
	<!--Geolocation/Map JS (This only needs to be included on the homepage)-->
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="<?php print base_path().path_to_theme() . '/js/google.maps.in_poly.js'?>"></script>
	<script type="text/javascript" src="<?php print base_path().path_to_theme() . '/js/community_boards.js'?>"></script>
	<script>
	$(document).ready( function() { initializeMap(); } )
	</script>

	<!--Slideshow, Bubble Animation & Main Dropdown (This is global)-->
	<script src="<?php print base_path().path_to_theme() . '/js/plugins.js'?>"></script>
	<script src="<?php print base_path().path_to_theme() . '/js/script.js'?>"></script>

	  <!--[if lt IE 7 ]>
	    <script src="js/libs/dd_belatedpng.js"></script>
	    <script> DD_belatedPNG.fix('img, .png_bg'); //fix any <img> or .png_bg background-img </script>
	  <![endif]-->

	</body>
	</html>
