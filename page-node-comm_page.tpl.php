<!doctype html>  
<?php require('header.tpl.php');?>

<section id="subpage_hero">	
	<div class="texture">
		<div class="main">
			<div class="subpage_header" id="connect_header">		
<!--Section Heading-->
				<h2>Connect</h2>
    		</div>
		</div>
		<div class="clearfix"></div>
	</div>	
</section>


<section id="group">
	
	<div class="main">
		
		<p class="breadcrumbs"><?php print $breadcrumb;?></p>

<div class="taximage">
tax images here:
<?php print $taxonomy_images[0] ?>
</div>

 <div id="subpage_content">

                        <h3><?php print $title;?></h3>
<!-- currently overriding all content on this page, so print content is not doing much -->
<div id="groupinfo">
		<h4>Browse The Community Groups </h4>
				
<?php
$view = views_get_view('groups_by_neighborhood');
print $view->execute_display('default', $args);
?>
</div>
		`
			</div>

				<div class="group_sidebar">
					<?php print $addinfo;?>
				</div>						
				<div class="group_sidebar">
					<?php print $sideright;?>		
				</div>
		
			<div id="groupsnear">
				<?php print $belowthefold;?>
	
</div>
</div>
<div class="clearfix"></div>
<section id="forums">
				<?php print $forumfooter;?>
</section>
	</div>
	<div class="clearfix"></div>
</section>
    
    
<!--Begin Footer-->
		<?php require('footer.tpl.php'); ?> 
<!--End Footer-->


	</div>
	</div>


<!-- Javascript at the bottom for fast page loading -->

	  <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
	  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
	  <script>!window.jQuery && document.write(unescape('%3Cscript src="js/libs/jquery-1.4.2.js"%3E%3C/script%3E'))</script>

		<!--Slideshow, Bubble Animation & Main Dropdown (This is global)-->
		<script src="js/plugins.js"></script>
		<script src="js/script.js"></script>


	  <!--[if lt IE 7 ]>
	    <script src="js/libs/dd_belatedpng.js"></script>
	    <script> DD_belatedPNG.fix('img, .png_bg'); //fix any <img> or .png_bg background-img </script>
	  <![endif]-->


	</body>
	</html>
