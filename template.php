<?php
// $Id: template.php,v 1.16.2.3 2010/05/11 09:41:22 goba Exp $

/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function phptemplate_body_class($left = null, $right = null) {
  $body_classes = array() ;
// Add a class that tells us whether we're on the front page or not.
  $body_classes[] = $variables['is_front'] ? 'front' : 'not-front' ;
// Add a class that tells us whether the page is viewed by an authenticated user or not.
  $body_classes[] = $variables['logged_in'] ? 'logged-in' : 'not-logged-in' ;

  if ($left != '' && $right != '') {
    $body_classes[] = 'sidebars';
  }
  else {
    if ($left != '') {
      $body_classes[] = 'sidebar-left';
    }
    if ($right != '') {
      $body_classes[] = 'sidebar-right';
    }
  }

  $body_classes[] = $variables['title'] ? 'title-' . form_clean_id(drupal_strtolower($variables['title'])) : 'no-title' ;
  
  if(arg(1)) {
     $body_classes[] =  'path-' . arg(0);
  }
  
  $body_classes[] = (arg(1)) ? 'path-' . form_clean_id(implode("-",arg())) : 'no-nid' ;  
  
  print ($body_classes[0]) ? ' class="'. implode(" ",$body_classes) .'"' : null;
}


/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}
function sunyc_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    $breadcrumb[0] = l('SpeakUp',NULL);
    return '<div class="breadcrumb">'. implode(' » ', $breadcrumb) .'</div>';
  }
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}

/**
 * Returns the themed submitted-by string for the comment.
 */
function phptemplate_comment_submitted($comment) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

/**
 * Returns the themed submitted-by string for the node.
 */
function phptemplate_node_submitted($node) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}

/**
 * Generates IE CSS links for LTR and RTL languages.
 */
function phptemplate_get_ie_styles() {
  global $language;

  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/fix-ie.css" />';
  if ($language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="all">@import "'. base_path() . path_to_theme() .'/fix-ie-rtl.css";</style>';
  }

  return $iecss;
}

/**
 * Vars for printing taxonomy images. 
 */ 

function _phptemplate_variables($hook, $vars) {
  if ($hook == 'node') {
    if (module_exist("taxonomy_image")) {
       foreach (taxonomy_node_get_terms($vars['node']->nid) as $term) {
        $vars['taxonomy_images'][] = taxonomy_image_display($term->tid, "alt='$term->name'");
       }
    }
  }
return $vars;
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();

  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }

  if(arg(0) == 'taxonomy'){
    $suggestions = array('node-taxonomy'); //So, node-taxonomy.tpl.php will be known afterwards.
    $vars['template_files'] = array_merge($vars['template_files'], $suggestions);
  }
}

function sunyc_preprocess_page(&$variables) {

  if ($variables['node']->type != "") {
    $variables['template_files'][] = "page-node-" . $variables['node']->type;
  }
if (module_exists('path')) {
    $alias = drupal_get_path_alias(str_replace('/edit','',$_GET['q']));
    if ($alias != $_GET['q']) {
      $template_filename = 'page';
      foreach (explode('/', $alias) as $path_part) {
        $template_filename = $template_filename . '-' . $path_part;
        $vars['template_files'][] = $template_filename;
      }
    }
  }
}

/** 
 * Allows overriding node.tpl.php by nid
 */ 
function sunyc_preprocess_node(&$variables) {
  $node  = $variables['node'];
  if ( !empty($node) ) {
    $variables['template_files'][] = "node-" . $node->nid;
  }
}



/**
 * Add a "Comments" heading above comments except on forum pages.
 */
function garland_preprocess_comment_wrapper(&$vars) {
  if ($vars['content'] && $vars['node']->type != 'forum') {
    $vars['content'] = '<h2 class="comments">'. t('Comments') .'</h2>'.  $vars['content'];
  }
}

// Overrides specific to SpeakUpNYC theme 

/**
* Override or insert PHPTemplate variables into the search_theme_form template.
*
* @param $vars
*   A sequential array of variables to pass to the theme template.
* @param $hook
*   The name of the theme function being called (not used in this case.)
* NOTE: this function should really be calling if ($variables['is_front'] !=1 to not impeed front page load performance
*/
function phptemplate_preprocess_search_theme_form(&$vars, $hook) {
 
  // Modify elements of the search form
  unset($vars['form']['search_theme_form']['#title']);
}



function sunyc_preprocess_search_theme_form(&$vars, $hook) {
  
  // Modify elements of the search form
  $vars['form']['search_theme_form']['#title'] = t('Search mysite.com');
  
  // Set a default value for the search box
  $vars['form']['search_theme_form']['#value'] = t('Search');
  
  // Add a custom class to the search box
  $vars['form']['search_theme_form']['#attributes'] = array('class' => t('cleardefault'));
  
  // Change the text on the submit button
  $vars['form']['submit']['#value'] = t('Go');
  
  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_theme_form']['#printed']);
  $vars['search']['search_theme_form'] = drupal_render($vars['form']['search_theme_form']);
  
  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);
  
  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}