<!doctype html>  

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ --> 
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<html xmlns:fb="http://www.facebook.com/2008/fbml" 
                    xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" 
                    lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">




<head><?php print $head ?>

  <meta charset="utf-8">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" href="/apple-touch-icon.png">

<link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:regular,bold' rel='stylesheet' type='text/css'>

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame 
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?php print $head_title ?></title>
  <meta name="description" content="Tagline goes here">
  <meta name="author" content="Balance Cooperative">
  <meta name="author" content="Drupal theme by Forest Mars">

  <!--  Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->


  <!-- CSS : implied media="all" -->
  <link rel="stylesheet" href="style.css?v=2">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->
 
  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="<?php print base_path().path_to_theme() . '/js/libs/modernizr-1.6.min.js' ?>"></script>
  
    <!--  Drupal styles and scripts go last so overrides work correctly -->
	<?php print $styles ?>
	<?php print $scripts ?>
</head>

<body<?php print phptemplate_body_class($left, $right); ?>>

<div id="header-region" class="clear-block"><?php print $header; ?></div>	
<!-- For some reason the admin_menu bar is not appearing in the theme yet -->

<div id="container">
		
	<div class="texture">
<!--Start Header-->			
		<header>
			<div id="header">
													
				<div class="main">
 <?php if (!empty($searchbox)) :  print '<div id="page_searchform">' . $searchbox . '</div>' ?>      <?php endif; ?>


					<div id="logo">
						<div id="logo-floater">
						<?php
        		// Prepare header
	       		  $site_fields = array();
		          if ($site_name) {
		            // $site_fields[] = $site_name;
		          }
		          if ($site_slogan) {
		            $site_fields[] = check_plain($site_slogan);
		          }
    		      $site_title = implode(' ', $site_fields);
        		  if ($site_fields) {
      		      $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
    		      }
   			      $site_html = implode(' ', $site_fields);
				
		          if ($logo || $site_title) { 
		          
    		        print '<h1 class="irk"><div class="vatb"><a href="'.$front_page. '" title="'. $site_title .'"></div>	';
        		    if ($logo) {
            		  print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            		}
           		 	print $site_html . '</a></h1>';     		 	
          			
          			}
        			?>
       				</div>
				</div><!-- /main-->
<nav>
	<?php print $navregion;?>     
			<?php print $topnav;?>
			<!-- ?php if (isset($primary_links)) : ? -->
    			<!-- ?php print theme('links', $primary_links, array('class' => 'links primary-links')) ? -->
    		<!-- ?php endif; ? -->
</nav>
				<div class="clearfix"></div>
</div></div>
</header>
<!--End Header-->

