
    
<!--Begin Footer-->
		<footer>
			<div id="footer">
				<div class="texture">

				<section id="sub_footer">
					<div class="texture">
						<div class="main">
							<div id="about">
								<?php print $triptych1;?>
							</div>
							<div id="contactus">
								<?php print $triptych2;?>
							</div>
							<div id="signup">
								<?php print $triptych3;?>
							</div>

							<!--End Sign Up for Newsletter Form-->
		    			</div>

						<div class="clearfix"></div>

					</div>	

				</section>

				<div class="main">
					<?php print $footer_message . $footer ?>	
					
					<!-- temporarily including sponsor logos in template. this should be moved to drupal footer. -->
					
						<h6>Partners:</h6>

					<ul>
							<li><id="mbp"><a href="http://www.mbpo.org/"><img src="<?php print base_path().path_to_theme() . '/images/logo_bp.png'?>" alt="Manhattan Borough President"/></a></li>
							<li id="openplans"><a href="http://www.openplans.org"><img src="<?php print base_path().path_to_theme() . '/images/logo_openplans.png'?>" alt="Open Plans"/></a></li>
							<li id="mnn"><a href="http://www.mnn.org"><img src="<?php print base_path().path_to_theme() . '/images/logo_mnn.png'?>" alt="MNN"/></a></li>
							<li id="infinopolis"><a href="http://infinopolis.com/"><img src="<?php print base_path().path_to_theme() . '/images/logo_infinopolis.png'?>" alt="Infinopolis"/></a></li>
							<li id="nycwiki"><a href="http://nycwiki.org/wiki/Main_Page"><img src="<?php print base_path().path_to_theme() . '/images/logo_nycwiki.png'?>" alt="NYC Wiki"/></a></li>
							<li id="clickfix"><a href="http://www.seeclickfix.com/"><img src="<?php print base_path().path_to_theme() . '/images/logo_seeclickfix.png'?>" alt="SeeClickFix"/></a></li>
					</ul>

					<p class="copy">&copy; Copyright 2006 The Manhattan Borough President’s Office</p>

					<div id="balance"><a href="http://speakupnyc.org/acknowledgements">Acknowledgements</a></div>

					<div class="clearfix"></div>	
				</div>
					
					
			</div>
			</div>
		</footer>
<!--End Footer-->


	</div>
	</div>


<!-- Javascript at the bottom for fast page loading -->

	  <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
	  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.js"></script>
	  <script>!window.jQuery && document.write(unescape('%3Cscript src="js/libs/jquery-1.4.2.js"%3E%3C/script%3E'))</script>

		<!--Slideshow, Bubble Animation & Main Dropdown (This is global)-->
		<script src="<?php print base_path().path_to_theme() . '/js/plugins.js'?>"></script>
		<script src="<?php print base_path().path_to_theme() . '/js/script.js'?>"></script>


	  <!--[if lt IE 7 ]>
	    <script src="js/libs/dd_belatedpng.js"></script>
	    <script> DD_belatedPNG.fix('img, .png_bg'); //fix any <img> or .png_bg background-img </script>
	  <![endif]-->


	</body>
	</html>
